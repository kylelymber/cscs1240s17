import javax.swing.JPanel;
import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;

public class Background 
{
	private int WIDTH;
	private int HEIGHT;
	private BufferedImage bg;
	
	public Background(int width, int height)
	{
		WIDTH = width;
		HEIGHT = height;
		try{
			bg = ImageIO.read(new File("Background.PNG"));
		}catch(IOException e){
			e.printStackTrace();
		}
	}
	
	public void myPaint(Graphics g,int x,int y)
	{
		g.drawImage(bg.getScaledInstance(WIDTH,HEIGHT, Image.SCALE_SMOOTH),x,y,null);
	}
}