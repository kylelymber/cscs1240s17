import javax.swing.JFrame;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.lang.Thread;

public class Game extends JFrame
{
	private static int WIDTH = 576;
	private static int HEIGHT = 1024;
	
	public Game()
	{
		setTitle("Flappybird Clone");
		setDefaultCloseOperation(EXIT_ON_CLOSE);		
		setFocusable(true);
		setPreferredSize(new Dimension(WIDTH,HEIGHT));
		setLocationRelativeTo(null);
		setMaximizedBounds(new Rectangle(0,0,WIDTH,HEIGHT));
		setVisible(true);
	}
	
	public static void main(String[] args)
	{
		Game game = new Game();
		Panel panel = new Panel(WIDTH,HEIGHT);
		game.setContentPane(panel);
		game.addKeyListener(panel);
		while(true)
		{
			panel.repaint();
		}
	}
}