import javax.swing.JFrame;
import javax.swing.JPanel;
import java.lang.Thread;
import java.awt.Color;
import java.awt.event.*;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.util.Random;

public class Panel extends JPanel implements KeyListener
{
	private int BGX 		  = 0, BGY = 0;
	public int BirdX          = 200, BirdY = 200;
	private static int WIDTH  = 576;
	private static int HEIGHT = 1024;
	private double speed 	  = 1.0;
	private double grav 	  = 0.1;
	private boolean canJump   = true;
	private int PipeX 		  = 0,PipeX2 = 600,PipeY = 0;
	private int minimum 	  = -150;
	private Random rand       = new Random();
	private int num           = rand.nextInt(300)-150;
	private int num2		  = rand.nextInt(300)-150;
	private boolean playing   = true;
	private boolean passed    = false;
	
	Background background;
	Background background2;
	Bird bird;
	Pipe pipe;
	Pipe pipe2;
	
	public Panel(int width,int height)
	{
		background  = new Background(width,height);
		background2 = new Background(width,height);
		bird        = new Bird();		
		pipe        = new Pipe();
		pipe2       = new Pipe();
		addKeyListener(this);
	}
	
	public void paintComponent(Graphics g)
	{	
		background.myPaint(g,BGX,BGY);
		background2.myPaint(g,BGX+WIDTH,BGY);
		bird.myPaint(g,BirdX,BirdY);
		pipe.myPaint(g,PipeX,PipeY+num,WIDTH,HEIGHT);
		pipe2.myPaint(g,PipeX2,PipeY+num2,WIDTH,HEIGHT);
		g.dispose();
		if(playing)
			move();		
	}
	public void move()
	{
		BGX--;
		PipeX--;
		PipeX2--;
		if(BGX < -WIDTH)
			BGX = 0;
		if(PipeX < -WIDTH-50)
		{
			PipeX = WIDTH;
			num = rand.nextInt(300)-150;
		}
		if(PipeX2 < -WIDTH-50)
		{
			PipeX2 = WIDTH;
			num2 = rand.nextInt(300)-150;
		}
		
		if(PipeX2-200 < BirdX && passed == false)
		{
			System.out.println("You dead");
			passed = true;
		}
		if(PipeX < BirdX && passed == false)
		{
			System.out.println("You dead");
			passed = true;
		}
		if((PipeX2-199 > BirdX || PipeX-199 > BirdX) && passed == true)
			passed = false;
		

		/*
		System.out.println("Bird ("+BirdX+","+BirdY+")");
		System.out.println("Pipe 1 ("+PipeX+","+(PipeY+num)+")");
		System.out.println("Pipe 2 ("+PipeX2+","+(PipeY+num2)+")");*/
		
		speed = speed+grav;
		BirdY += (int)speed;
	}
	
	public void keyTyped(KeyEvent e)
	{
		
	}
	public void keyPressed(KeyEvent e)
	{
		if(e.getKeyCode() == KeyEvent.VK_SPACE && canJump)
		{
			System.out.println("Jump");
			speed = -6;
			canJump = false;
		}
	}
	public void keyReleased(KeyEvent e)
	{
		if(e.getKeyCode() == KeyEvent.VK_SPACE && !canJump)
			canJump = true;
	}
}