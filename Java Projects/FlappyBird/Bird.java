import javax.swing.JPanel;
import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;

public class Bird
{
	private BufferedImage bird;
	private int BirdW;
	private int BirdH;
	
	public Bird()
	{	
		try{
			bird  = ImageIO.read(new File("Bird.png"));
			BirdW = bird.getWidth();
			BirdH = bird.getHeight();
		}catch(IOException e){
			e.printStackTrace();
		}
	} 
	
	public void myPaint(Graphics g,int x,int y)
	{
		g.drawImage(bird.getScaledInstance(BirdW*2,BirdH*2,Image.SCALE_SMOOTH),x,y,null);
	}
}