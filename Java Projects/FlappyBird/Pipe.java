import javax.swing.JPanel;
import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;

public class Pipe 
{
	public int PUWidth;
	public int PUHeight;
	public int PDWidth;
	public int PDHeight;
	private int pipeHeight;
	private BufferedImage pipeDown;
	private BufferedImage pipeUp;
	private Image scaledDown;
	private Image scaledUp;
	private int GAP = -10;
	private int top = 200;
	private int bottom = 650;
	private int topY, bottomY;
	
	public Pipe()
	{
		try{
			pipeDown = ImageIO.read(new File("PipeDown.PNG"));
			pipeUp   = ImageIO.read(new File("PipeUp.PNG"));
			PUHeight = pipeUp.getHeight();
			PUWidth  = pipeUp.getWidth();
			PDHeight = pipeDown.getHeight();
			PDWidth  = pipeDown.getWidth();
		}catch(IOException e){
			e.printStackTrace();
		}
		scaledDown = pipeDown.getScaledInstance((PDWidth*2)*2,(PDHeight*2)*2, Image.SCALE_SMOOTH);
		scaledUp   = pipeUp.getScaledInstance((PDWidth*2)*2,(PDHeight*2)*2, Image.SCALE_SMOOTH);
	}
	
	public void myPaint(Graphics g,int x,int y, int width, int height)
	{
		g.drawImage(scaledDown,x+width-110,y-(top+GAP),null);
		g.drawImage(scaledUp,x+width-110, y+(bottom+GAP),null);

	}
}