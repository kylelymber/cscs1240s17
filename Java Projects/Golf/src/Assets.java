import java.awt.image.BufferedImage;

public class Assets
{
	private static final int width = 81, height = 117;
	public static BufferedImage[] card = new BufferedImage[55];
	public static BufferedImage tester;
	public static void init()
	{
		SpriteSheet sheet = new SpriteSheet(ImageLoader.loadImage("../res/textures/Deck.png"));
		int j = 0;
		for(int i = 0; i < 54; i++)
		{
			if((i%13) == 0 && j < 5 && i != 0)
				j+=1;
			card[i] = sheet.crop(width*(i%13),height*j,width,height);
		}
		card[54] = sheet.crop(width,height*4,width,height);
	}
}