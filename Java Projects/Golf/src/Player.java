import java.awt.Graphics;

public class Player
{	
	private Card[] hand = new Card[9];
	private Card selected, swapCard;
	
	public Player()
	{
	
	}
	
	public void deal(Card[] deck)
	{
		// select 9 cards to put into players hand
		for(int i = 0; i < 9; i++)
			hand[i] = deck[i];
		
	}
	
	public void update()
	{
	
	}
	public void render(Graphics g)
	{
		for(int i = 0; i < 9; i++)
			g.drawImage(hand[i].render(g,20+(i*81+20),300));
	}
	
	public void swap()
	{
		// take selected card move to hand move hand card to discard pile
	}
	
	
	public Card getSelected(){return selected;}
	public void setSelected(Card selected){selected = selected;}
}