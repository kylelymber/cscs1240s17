import java.awt.Graphics;
import java.awt.event.*;

public class Card implements MouseListener
{
	private int x, y, index;
	private boolean hovering;
	public Card(int index,int x, int y)
	{
		this.index = index;
		this.x = x;
		this.y = y;
		hovering = false;
	}
	public void update()
	{
	
	}
	public void render(Graphics g, int xx, int yy)
	{
		g.drawImage(Assets.card[index],xx,yy,null);
	}
	
	/*			Mouse Methods            */
	public void mouseClicked(MouseEvent e)
	{

	}
	public void mouseEntered(MouseEvent e)
	{

	}
	public void mouseExited(MouseEvent e)
	{

	}
	public void mousePressed(MouseEvent e)
	{
	
	}
	public void mouseReleased(MouseEvent e)
	{
	
	}
	/*			Mouse Methods            */
}