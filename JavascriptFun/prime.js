function primeThrough()
{
	// counts every succesful prime number
	var totalPrimes = 0;
	
	// gets the user input from the form and input box
	var checkThrough = document.getElementById("input").elements["primeNumber"].value;
	
	var dividend = 3;
	var divisor = 2;
	var output = "";
	
	// flag whether the number is prime or not
	var primeness = true;
	
	// first loop is the max number that increases to the checkthrough number
	for(dividend=3;dividend<=checkThrough;dividend++)
	{
		// second loop checks if it is prime from 2 - the dividend
		for(divisor=2;divisor<=dividend;divisor++)
		{			
			if(dividend%divisor === 0 && dividend != divisor)
			{
				// sets the primeness flag if it is not prime!
				primeness = false;
				break; // breaks out of the current itteration of the inner loop becuase
					   // it was already proven that the number is not prime!
			}
			
		}
		// once the inner loop finishes check the primeness flag if it is prime do this stuff
		if(primeness === true)
		{
			// add more numbers to the list of current numbers!
			output = output+", "+dividend;
			// add 1 to the total number of primes!
			totalPrimes++;
		}
		// reset the value of the flag for the next itteration
		primeness = true;
	}
	totalPrimes += 1; // account for the 2 that we manually printed out.
	
	// print out to the website document page
	document.write("Brute force attempt<br><br>");
	document.write("There are "+totalPrimes+" Total prime numbers out of "+checkThrough+"<br><br>");
	document.write("2"+output);
}