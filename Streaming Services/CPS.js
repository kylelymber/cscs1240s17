// declare your variables starting with var for variable
// linked list function to remember who has created an account
// variables
var current;
var destination;
var cbuild;
var dBuild;
var cFloor;
var dFloor;
var choice;
var currentUser = null;
var UserList = new Users();

function newAccount()
{
	var email = document.forms["newAcc"][ "email"].value;
	var email2 = document.forms["newAcc"][ "repeatEmail"].value;
	var userName = document.forms["newAcc"]["newUsername"].value;
	var pass1 = document.forms["newAcc"]["newPass"].value;
	var pass2 = document.forms["newAcc"]["newPass2"].value;
	var ret = true;
	
	if(email.indexOf("@corning-cc.edu") == -1)
	{
		alert("Email is not from CCC");
		ret = false;
	}
	else if(email != email2)
	{
		alert("Emails did not match");
		ret = false;
	}
	if(pass1 != pass2)
	{
		alert("Passwords did not Match");
		ret = false;
	}
	if(pass1.length < 8)
	{
		alert("Passwords were not long enough");
		ret = false;
	}	
	if(search(pass1,userName) != null)
	{
		alert("an account already exists with that userName");
		ret = false;
	}
	if(ret)
	{
		//Users.add(userName,pass1,email);
		alert("The account was successfully created!");
	}
	return ret;		
}

function User(userName,password,email)
{
	this.email = email;
	this.userName = userName;
	this.password = password;
	this.next = null;
}
function Users()
{
	this.head = null;
	this._length = 0;
}
Users.prototype.add = function(userName,password,email)
{
	var newAcc = new User(userName,password,email);
		if(this.head == null)
			this.head = newAcc;
		else 
		{
			var tmp = this.head;
			while(tmp.next)
			{
				tmp = tmp.next;
			}
			tmp.next = newAcc;
			this._length++;
		}
}
Users.prototype.search = function(password,userName)
{
	if(this.head != null)
	{
		var tmp = this.head;
		while(tmp.next)
		{
			if(tmp.userName == userName && tmp.password == password)
				break;
			tmp = tmp.next;
		}
	}
	else
		var tmp = null;
	return tmp;
}

// loop to create and login
function login()
{
	while(currentUser == null)
	{
		while(currentUser != null)
		{
			choice = prompt("Would you like to get \"directions\" or \"logout\"?");
			if(choice == "logout")
				currentUser = null;
			else if(choice == "directions")
			{
				// tell the user how to operate the program.
				alert("Input in the form of C102 or S201 for both the current and destination location");
				alert("Make sure to use CAPITAL LETTERS when typing the first letter");

				// get user input using the prompt function.
				current     = prompt("What class room are you in?");
				destination = prompt("What class room are you going to?");

					// obtain the first character from the input string
				// this would correspond to the building letter of the room number
				cBuild = current.charAt(0);
				dBuild = destination.charAt(0);

				// gets the second character from the input string
				// this would correspond to the floor of the room number
				cFloor = current.charAt(1);
				dFloor = destination.charAt(1);

				// Checks current floor, tells you to go outside.
				if(cFloor > 1)
					alert("make your way down the stairs to the first floor");
				else if(cFloor < 1)
					alert("make your way up the stairs to the first floor");
				else 
					alert("leave the building");

				// checks destination floor tells you to go upstairs or not.
				if(dFloor == 2)
					alert("make your way to the second floor of the of the "+"\""+dBuild+"\""+" building");
				else if(dFloor == 3)
					alert("make your way to the third floor of the of the "+"\""+dBuild+"\""+" building");
				else if(dFloor == 0)
					alert("make your way to the bottom floor of the of the "+"\""+dBuild+"\""+" building");
				else 
					alert("go find your room here some where");
			}
			else
				alert("invalid input");
		}
	}
}